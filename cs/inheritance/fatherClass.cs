﻿using System;
using System.Xml.Schema;

namespace cs.inheritance
{
    public class FatherClass
    {
        //this class is for Father , the Father name is mamad!
        //====================================================
        //Fathers trait:

        protected string firstName;

        protected string lastName;

        protected int age;
        //what the hell is that ,of course he's Married :\
        protected bool married;

        public string _firstName
        {

            get { return firstName; }
            set { firstName = value; }
        }

        public string _lastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public int _age
        {
            get { return age; }
            set { age = value; }
        }

        public string _married
        {
            get
            {
                if (married == true) return "Motahel";
                else return "Mojarad";
            }
            set
            {
                if (value == "yes") married = true;
                else married = false;
            }
        }

        //some of the father abilitys


        public static void Cook() => Console.WriteLine("faghat baladm tokhmorghe aab paz dorost konam !...");
        //baraye in static neveshtam ke to ersbari bebinid che shekli mishi vagarna kar alaki yi hastesh!!!


        public virtual void Drive() => Console.WriteLine("drive carefully...!");



    }
}