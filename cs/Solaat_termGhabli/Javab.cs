﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs.Solaat_termGhabli
{
    //Soal 1, Ghemat Aleph
    /// <summary>
    /// //
    /// </summary>
    class CompareETH
    {
        public static int CompareMethod(Comparble a, Comparble b)
        {
            if (a.OCompare(b) == a) return 1;
            else if (a.OCompare(b) == b) return -1;
            else return 0;
        }
    }

    abstract class Comparble
    {
        abstract public Comparble OCompare(Comparble a);
    }

    //Ghesmat Be
    class Student : Comparble
    {
        int avg;

        public override Comparble OCompare(Comparble a)
        {
            Student s = (Student)a;
            if (this.avg > s.avg) return this;
            else if (this.avg < s.avg) return s;
            return null;
        }
    }

    //Soal 2,Ghesmate Aleph

    class Employee
    {
        protected string name;
        protected string Lname;
        protected string Ncode;
        protected string Lcode;
        protected int pay;
        protected Date d;

        public int PastMonth
        {
            get
            {
                int m = 11 - d.month, y = 1396 - d.year;
                if (m > 0)
                {
                    return (y - 1) * 12 + (m + 12);
                }
                else
                {
                    return y * 12 + m;
                }
            }
        }

        public override string ToString()
        {
            return name + "-" + Lname + "-" + Ncode + "-" + d.year;
        }

        public virtual string Payment()
        {
            string s;
            s = (pay * PastMonth).ToString();
            return s;
        }
    }

    //Ghesmate Be

    class Accountan : Employee
    {
        int ShomareOtagh;
        public override string ToString()
        {
            return name + "-" + Lname + "-" + Ncode + "-" + d.year + "-" + ShomareOtagh;
        }
        public override string Payment()
        {
            string s;
            s = (pay * PastMonth * 2).ToString();
            return s;
        }
    }

    //Soal 3
    class Company
    {
        Stack<Employee> list;
        public void Employe(Employee e)
        {
            list.Push(e);
        }
        public Employee Retire()
        {
            list.Peek().ToString();
            return list.Pop();
        }
    }


    //Class Date baraye tarikh

    class Date
    {
        public int day;
        public int month;
        public int year;
        public Date(int day, int month, int year)
        {
            this.day = day;
            this.month = month;
            this.year = year;
        }
    }
}
