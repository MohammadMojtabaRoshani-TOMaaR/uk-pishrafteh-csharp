﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Tamrin Ejtema va Eshterak va soal 3 mid-term
/// </summary>

namespace Exercies
{

    //Interface

    interface IComparable
    {
        int ICompare(IComparable a);
    }
    class Set
    {

        // Fields

        private int Count = 0;
        private IComparable[] CP;

        // Prop

        public int Length
        {
            get
            {
                return Count;
            }
        }

        private bool IsFull
        {
            get
            {
                return CP.Length == Count;
            }
        }

        private bool IsEmpty
        {
            get
            {
                if (Count == 0) return true;
                else return false;
            }
        }
        // Constractor

        public Set(int Len)
        {
            this.CP = new IComparable[Len];
        }

        // Indexer baraye dasteresi rahat tar

        private IComparable this[int indexer]
        {
            get
            {
                return CP[indexer];
            }
            set
            {
                CP[indexer] = value;
            }
        }


        // InSert Function

        public void InSert(IComparable Object)
        {
            if (IsFull == false)
            {
                if (Count != 0)
                {

                    int i;
                    for (i = Count - 1; Object.ICompare(CP[i - 1]) == -1; i--)
                    {
                        CP[i] = CP[i - 1];
                    }
                    CP[i+1] = Object;
                    Count++;
                }
                else
                {
                    CP[0] = Object;
                    ++Count;
                }
            }
            else throw new Exception("Full!");
        }

        // ReMove Function

        public void ReMove()
        {
            if (IsEmpty == false)
            {
                CP[Count] = null;
                Count--;
            }
            else throw new Exception("it's Already Empty!");
        }

        // Union Func : its Static!
        //Also Because Set is Sorted, We can Get Union Easily

        public static Set Union(Set S1, Set S2)
        {
            int k = 0;
            Set Union = new Set(S1.Length + S2.Length - 1);
            for (int i = 0, j = 0; i < S1.Length - 1 || j < S2.Length - 1;)
            {
                if (i >= S1.Length)
                {
                    Union.InSert(S2[j]);
                    j++;
                    k++;
                }
                if (j >= S2.Length)
                {
                    Union.InSert(S1[i]);
                    i++;
                    k++;
                }
                if (S1[i].ICompare(S2[j]) == 1)
                {
                    Union.InSert(S1[i]);
                    i++;
                    k++;
                }
                else if (S1[i].ICompare(S2[j]) == -1)
                {
                    Union.InSert(S2[j]);
                    j++;
                    k++;
                }
                else
                {
                    Union.InSert(S2[j]);
                    i++;
                    j++;
                    k++;
                }

            }
            return Union;
        }

        // InTersection Func : its Static!

        public static Set InTersection(Set S1, Set S2)
        {
            int k = 0;
            Set Intersection = new Set(Math.Max(S1.Length, S2.Length));
            for (int i = 0; i < S1.Length; i++)
            {
                for (int j = 0; j < S2.Length; j++)
                {
                    if (S1[i].ICompare(S2[j]) == 0)
                    {
                        Intersection.InSert(S2[j]);
                        k++;
                    }
                }
            }
            return Intersection;
        }

    }
    class Person : IComparable
    {
        int ID;

        public int ICompare(IComparable y)
        {
            Person L = (Person)y;
            if (this.ID == L.ID) return 0;
            else return -1;
        }
    }
    class Student : IComparable
    {
        int StdNo;
        public int ICompare(IComparable y)
        {
            Student L = (Student)y;
            if (this.StdNo == L.StdNo) return 0;
            else return -1;
        }
    }
    // In Main :
    //Now We Can Easily Make a Set of Person Or Student Object And We Can Unite them with Set.Union(a,b)
    // Let a and b be two Set!!!!
    //Example:
    // IComparable a = new Set(10); 10 is maximum size of set
    // IComparable b = new Set(6); 6 is maximum size of set
    //Fill with Student Object or Person your-self
    //Then Use
    // Set c = Set.Union(a,b);
    // Set d = Set.InTersection(a,b);
    //Done :)
}
