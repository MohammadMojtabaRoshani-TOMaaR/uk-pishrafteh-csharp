﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;


namespace cs.hale_mesal_marboot_be_date_and_time
{
    class DateAndTime
    {

        private int day;
        private int month;
        private int year;

        private static  int[] MonthDays  = {
            0,31,31,31,31,31,31,30,30,30,30,30,30
        };

        
        public int _day
        {
            get { return day; }
            set {
                if (value >= 0 && value<MonthDays[_month])
                {
                    this.day = value;
                }
                else
                {
                    day = 1;
                }
            }
        }

        public int _month
        {
            get { return month;}
            set {
                if (value>0 && value <13)
                {
                    month = value;
                }
                else
                {
                    month = 1;
                }
            }
        }

        public int _year
        {
            get { return year;}
            set
            {
                if (value>=1300)
                {
                    year = value;
                }

            }
        }

        //constractor:

        public DateAndTime(int Day,int Month,int Year)
        {

            _month = Month;
            _day = Day;
            _year = Year;

        }

        public DateAndTime(DateAndTime dateAndTime)
        {

            
            _month = dateAndTime._month;
            _day = dateAndTime._day;
            _year = dateAndTime._year;
            
        }

        public string ToString()
        {
            return _day.ToString("00") +  "/" + _month.ToString("00") + "/" + _year.ToString("00");

        }

        private bool EndOfMonth()
        {
            //agar akharin rooz mah bood :
            return _day == MonthDays[_month];

        }

        private void CheckDate()
        {
            if (_month == 12 && EndOfMonth())
            {
                _year++;
                _month = 1;
                _day = 1;

            }
            else if (EndOfMonth())
            {
                _month++;
                _day = 1;
            }
            else
            {
                _day++;
            }
        }

        public DateAndTime AddDay(int day)
        {
            DateAndTime dati = new DateAndTime(this);

            for (int i = 1; i <= day; i++)
            {
                dati.CheckDate();
            }

            return dati;

        }

        //operator:

        public static DateAndTime operator +(DateAndTime innerDateAndTime,int day)
        {
            DateAndTime dati = new DateAndTime(innerDateAndTime);

            for (int i = 1; i <= day; i++)
            {
                dati.CheckDate();
            }

            return dati;
        }
        public static DateAndTime operator +(int day,DateAndTime innerDateAndTime)
        {
            DateAndTime dati = new DateAndTime(innerDateAndTime);

            for (int i = 1; i <= day; i++)
            {
                dati.CheckDate();
            }

            return dati;
        }

        public static bool operator >(DateAndTime a,DateAndTime b)
        {
            return ((a._year>b._year || a._year == b._year && a._month>b._month || a._year == b._year && a._month > b._month && a._day>b._day));

        }

        public static bool operator <(DateAndTime a, DateAndTime b)
        {
            return ((a._year < b._year || a._year == b._year && a._month < b._month || a._year == b._year && a._month < b._month && a._day < b._day));

        }



        //Don't Work!
        public static DateAndTime operator ++(DateAndTime a )
        {
            DateAndTime dati = new DateAndTime(a);
            
            dati.CheckDate();
          
            return dati;
        }
        //explicit
        public static explicit operator int(DateAndTime x)
        {
            int n = 0;
            for (int i = 1; i < x.month; i++)
            {
                n += MonthDays[i];
                n += x._day;

            }

            return n;
        }
        //implicit
        public static implicit operator string(DateAndTime x)
        {
            return x.ToString();
        }

        //if it'sthe first day of year it will return true and else false

        public static bool operator true(DateAndTime x)
        {

            return (x._day == 1 && x._month==1);

        }
        public static bool operator false(DateAndTime x)
        {

            return !(x._day == 1 && x._month == 1);

        }
        

        ~DateAndTime()
        {
            
        }
    }
}
