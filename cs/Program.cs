﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.Remoting.Channels;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using cs.Abstract;
using cs.Delegate;
using cs.Event;
using cs.Foreach;
using  cs.hale_mesal_marboot_be_date_and_time;
using cs.inheritance;
using cs.INterface;
using cs.Params;
using cs.ref_and_out;

namespace cs
{
    class Program
    {
        //add delegate here:
        public delegate void delegated(double grade);
        //event :
        public delegate void delegated2(string userName);
        event delegated2 newEvent;

        
        EventEx dataEx = new EventEx(); 
        public Program()
        {
            newEvent += new delegated2(dataEx.SayGoodLuck);
            newEvent += dataEx.SayGoodBy;

        }


        static void Main(string[] args)
        {
            //print sth on console 
            Console.WriteLine("Hello cs...");
            //======================================
            //for loop
            //for (int i = 0; i <= 10; i++)
            //{

            //    Console.WriteLine(  i.ToString() );

            //}
            //=======================================
            // this part of code is for inheritance


            //FatherClass father = new FatherClass();

            ////mitonim object child ro dar reference pedar bezarim
            //FatherClass child = new ChildClass();

            //Console.WriteLine("father first name");
            //father._firstName = Console.ReadLine();
            //Console.WriteLine("father last name");
            //father._lastName = Console.ReadLine();
            //Console.WriteLine("father age");
            //father._age = int.Parse(Console.ReadLine());
            //Console.WriteLine("father is Married (yes or no)");
            //father._married = Console.ReadLine();

            //Console.WriteLine("==========now for child==========");

            //Console.WriteLine("child first name");
            //child._firstName = Console.ReadLine();
            //child._lastName = father._lastName;
            //Console.WriteLine("child age");
            //child._age = int.Parse(Console.ReadLine());
            //Console.WriteLine("child is Married (yes or no)");
            //child._married = Console.ReadLine();

            ////result for father:

            //Console.WriteLine("father :" +
            //                  father._firstName + "|" +
            //                  father._lastName + "|" +
            //                  father._age.ToString() + "|" +
            //                  father._married);
            //FatherClass.Cook();
            //father.Drive();

            //Console.WriteLine("====================");
            ////result for child:


            //Console.WriteLine("child :" +
            //                  child._firstName + "|" +
            //                  child._lastName + "|" +
            //                  child._age.ToString() + "|" +
            //                  child._married);

            //ChildClass.Cook();
            //child.Drive();

            //========================================
            //our foreach:

            //ForeachClass _collection = new ForeachClass();

            //var localCollection = _collection.number.GetEnumerator();

            //for (int i = 0; i < _collection.number.Count; i++)
            //{

            //    bool nextInt;
            //    int forLoopInt;
            //    nextInt = localCollection.MoveNext();
            //    if (nextInt)
            //    {
            //        forLoopInt = localCollection.Current;
            //        Console.WriteLine( forLoopInt.ToString() );
            //    }

            //}

            //========================================  
            //ref/out

            //ValuesClass v_alues = new ValuesClass();
            //             //MAGHADIR AVALIYE
            //Console.WriteLine("Refrence is :" + v_alues.a.ToString()+" and "+v_alues.b.ToString());
            //RefrenceCahanger(ref v_alues.a,ref v_alues.b);
            //Console.WriteLine("change Refrence by method :" + v_alues.a.ToString() + " and " + v_alues.b.ToString());

            //void RefrenceCahanger(ref int a ,ref int b)
            //  {
            //      a = 100;
            //      b = 200;

            //  }

            //Console.WriteLine( "=============" );
            //C_Refrence(out v_alues.c);
            //Console.WriteLine( v_alues.c );
            //Console.WriteLine("=============");

            //void C_Refrence(out int c)
            //{
            //    c = 300;
            //}
            //========================================
            //params

            //JustAMethod.WritePhoneNumbers("Mojtaba",9900002028,9210000181,9360009049,9140002811,9220005713);
            ////ta bi nahayat...
            //========================================

            //DateAndTime dateGetter1 = new DateAndTime(5,6,1378);


            ////Console.WriteLine(dateGetter1+5.ToString());
            //DateAndTime dateGetter2 = new DateAndTime(23,6,1378);
            //// Console.WriteLine(dateGetter2.AddDay(1).ToString());

            ////=======
            //if (dateGetter1 > dateGetter2)
            //{
            //    Console.WriteLine("yeeeees");
            //}
            //else
            //{
            //    Console.WriteLine( "noooooo" );
            //}
            ////=======
            ////explicit
            //int day = (int) dateGetter2;
            //Console.WriteLine(day);
            ////implicit
            //string day2 = dateGetter1;
            //Console.WriteLine(day2);

            //DateAndTime dateGetter3 = new DateAndTime(1, 1, 1378);

            //if (dateGetter3)
            //{
            //    Console.WriteLine("it's the new year!");
            //}
            //else
            //{
            //    Console.WriteLine( "it's not new !!!!" );
            //}
            ////========================================
            ////interface:

            //child childproperty = new child();

            //childproperty._firstName = "Mohammad Mojtaba";
            //childproperty._lastName = "Roshani";
            //childproperty._age = 20;
            //childproperty.Drive();            

            //childproperty.cookFromIFather();
            //childproperty.driveFromIFather();
            //========================================
            //abstract:

            //FutureFather  newFather = new FutureFather();

            //newFather._firstName = "Mh";
            //newFather._lastName = "R";
            //newFather._age = 50;
            //newFather._married = "Mojarad";

            //newFather.Write();
            ////========================================
            ////delegate:

            //DelegateEx dlEx = new DelegateEx();
            //dlEx.TargetDelegated += new delegated(dlEx.PassMath);
            //dlEx.TargetDelegated += dlEx.PishrafePass;
            //dlEx.TargetDelegated += dlEx.MoadelaatPass;
            //dlEx.TargetDelegated(3.214);
            //========================================
            Program callMethod = new Program(); 
            callMethod.EventCaller();
            

            //========================================
            //pause the console
            Console.ReadKey();

        }

        public void EventCaller()
        {
            EventEx fireevent = new EventEx();
            Console.WriteLine("Enter your name...");
            fireevent._userName = Console.ReadLine();
            newEvent(fireevent._userName);


        }

        
    }
}
