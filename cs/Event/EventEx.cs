﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static cs.Program;

namespace cs.Event
{
    class EventEx
    {

        private string userName;
        public string _userName
        {
            get { return userName; }
            set { userName = value; }

        }

         //event delegated2 newEvent; 
            //newEvent(userName);
        

        public void SayGoodLuck(string name)
        {

            Console.WriteLine("GoodLuck " + name);

        }
        public void SayGoodBy(string name)
        {

            Console.WriteLine("GoodBy " + name);

        }
    }
}
