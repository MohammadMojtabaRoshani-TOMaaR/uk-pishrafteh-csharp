﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cs
{
    /// <summary>
    /// class Date shabih saz tarikh hast va dar edame mibinim ke chetor az field va property estefade konim va mani harkodam chi hast
    /// </summary>
    class Date
    {

        //Fields

        private int day; // baraye rooz va dalil private bodan in ast ke az biroon nashe dastkarish kard
        private int month; // baraye mah
        private int year; // baraye sal

        //Property baraye control field ha

        public string Day
        {
            get { return day.ToString(); } // Property Day public ast chon dar Main ya ja haie digar az oon estefade mikonim,
                                           //matlob hast ke rooz be string dade shavad
        }
        public string Month
        {
            get { return month.ToString(); } // moshabeh rooz baraye mah
        }
        public string Year
        {
            get { return year.ToString(); }
        }

        public Date(int day, int month, int year) //Constructor
        {
            this.day = day;
            this.month = month;
            this.year = year;
        }

        // Hala biain ye method benevisim ke tarikh ro chap kone be sorat(yyyy/mm/dd)

        public void ShowDate()
        {
            Console.WriteLine(Year + "/" + Month + "/" + Day);
        }


        //In Main:
        //Date date = new Date(12, 4, 1900);
        //date.ShowDate();
    }
}
